<!--#include file="include/connect.asp"-->

<%
dim id
id=Request.QueryString("id")
if not IsNumeric(id) or id="" then
	response.redirect("index.asp")
end if

if Request.ServerVariables("REQUEST_METHOD")="POST" then 
	dim msv,tensv,quequan,ngaysinh,sql
	msv=Request.Form("msv")
	tensv=Request.Form("tensv")
	quequan=Request.Form("quequan")
	ngaysinh=Request.Form("ngaysinh")
	sql="Delete from sinhvien "
	sql=sql&" WHERE id="&id
	response.write sql
	conn.Execute(sql)
	Response.redirect("index.asp")
end if
sql="SELECT * FROM sinhvien WHERE id="&id
rs.open sql,conn
if rs.EOF then
	Response.redirect("index.asp")
end if
Dim title
title="Xóa sinh viên"
 %>
<!--#include file="include/header.asp"-->
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-push-3">
			<table class="table table-bordered table-hover">
				<tbody>
					<tr>
						<td>Mã sinh viên</td>
						<td><%=rs("msv") %></td>
					</tr>
					<tr>
						<td>Tên sinh viên</td>
						<td><%=rs("tensv") %></td>
					</tr>
					<tr>
						<td>Quê quán</td>
						<td><%=rs("quequan") %></td>
					</tr>
					<tr>
						<td>Ngày sinh</td>
						<td><%=rs("ngaysinh") %></td>
					</tr>
				</tbody>
			</table>

				<a href="index.asp" class="btn btn-primary">Trở về Trang chủ</a>
				<form class="pull-right" onsubmit="return check()" method="POST" action="">
				<input type="submit" class="btn btn-danger" value="Xóa">
				</form>
		</div>
	</div>
</div>

<script>
	function check() {
		var yes=confirm("Bạn có muốn xóa sinh viên này");
		if (yes==true) {
			return true;
		}
		return false;
	}
</script>
<!--#include file="include/footer.asp"-->