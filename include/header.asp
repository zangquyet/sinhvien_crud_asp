<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><% 
	Dim suppertitle
	suppertitle="HAND"
	if title="" then
		Response.Write suppertitle
	else
	Response.Write suppertitle &" | "&  title
	end if
	 %></title>
	<!-- Latest compiled and minified CSS & JS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-inverse" style="border-radius: 0px;">
	<div class="container-fluid">
		<a class="navbar-brand" href="index.asp">Quản lý sinh viên</a>
		<form method="POST" action="index.asp" class="navbar-form navbar-left">
	      <div class="form-group">
	        <input type="text" name="key" class="form-control" placeholder="Tên, quê quán, mã sinh viên">
	      </div>
	      <button type="submit" class="btn btn-success">Tìm kiếm</button>
	    </form>

	    <ul class="nav navbar-nav navbar-right">
	      <li><a href="addsv.asp">Thêm mới sinh viên</a></li>
	    </ul>
	</div>
</nav>